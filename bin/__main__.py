#!/usr/bin/python

import sys
import os

ARGS = sys.argv + ['']*10
SCRIPTS = [ name for name in os.listdir(ARGS[0]+'/..') if os.path.isfile( '%s/../%s/__init__.py' % (ARGS[0],name) ) ]

if ARGS[1] in ('', 'help', '--help'):
    if ARGS[2] in SCRIPTS:
        exit( __import__( ARGS[2] ).__doc__ )
    elif ARGS[2]:
        exit( "bh: '%s' is not a bh command. See 'bh --help'.\n" % ARGS[2] )
    else:
        print 'usage: bh <script> [<args>]\n'
        print 'The most commonly used bh scripts are:'
        for script in SCRIPTS:
            print '    ' + script
        print

elif ARGS[1] in SCRIPTS:
    exit( __import__( ARGS[1] ).main( ARGS[2:] ))
else:
    exit( "bh: '%s' is not a bh command. See 'bh --help'.\n" % ARGS[1] )

